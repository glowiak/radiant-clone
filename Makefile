CC=	gcc
CFLAGS=	-lraylib -O3
DESTDIR=	
PREFIX=	/usr/local

all: main

main:
	rm -f radiant-clone
	(cd src && ${CC} ${CFLAGS} -o ../radiant-clone main.c)

install: main
	mkdir -p ${DESTDIR}${PREFIX}/share/radiant-clone ${DESTDIR}${PREFIX}/bin
	cp -rfv textures/ ${DESTDIR}${PREFIX}/share/radiant-clone/textures
	cp -f radiant-clone ${DESTDIR}${PREFIX}/share/radiant-clone/radiant-clone
	echo "#!/bin/sh" > ${DESTDIR}${PREFIX}/bin/radiant-clone
	echo "(cd ${PREFIX}/share/radiant-clone && ./radiant-clone)" >> ${DESTDIR}${PREFIX}/bin/radiant-clone
	chmod 775 ${DESTDIR}${PREFIX}/bin/radiant-clone
