#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "raylib.h"

int width = 900;
int height = 600;

int main()
{
    int lscreen = 0;
    int mbsg = 0;
    int bspeed = 10;
    int score = 0;
    
    int Ialient_red[30];
    int Ialient_yellow[30];
    int Ialient_purple[30];
    int Iavrak[30];
    
    int speed = 5;
    int selIt = 0;
    int abspeed = 5;
    InitWindow(width, height, "Radiant clone");
    SetTargetFPS(60);
    
    int RedAlientX[30];
    int RedAlientY[30];
    
    int YellowAlientX[30];
    int YellowAlientY[30];
    
    Rectangle abb = { 350, 290, 360, 300 };
    Rectangle aob = { 10, height - 20, 25, height };
    Rectangle stButt = { width/2 - 100, 120, width/2 - 40, 125 };
    Texture2D bg = LoadTexture("textures/background.png");
    bg.width = width;
    bg.height = height;
    int health = 3;
    int posx = width/2;
    int posy = height - 40;
    Texture2D rocket = LoadTexture("textures/rocket_idle.png");
    rocket.width = 50;
    rocket.height = 50;
    
    Texture2D heart = LoadTexture("textures/rocket_idle.png");
    heart.width = 30;
    heart.height = 30;
    RedAlientX[0] = width/2;
    RedAlientY[0] = 20;
    Texture2D alient_red = LoadTexture("textures/alient_small_red.png");
    alient_red.width = 35;
    alient_red.height = 35;
    Texture2D alient_yellow = LoadTexture("textures/alient_small_yellow.png");
    alient_yellow.width = 35;
    alient_yellow.height = 35;
    YellowAlientX[0] = width/2 + 50;
    YellowAlientY[0] = 60;
    
    Texture2D bullet = LoadTexture("textures/ammo.png");
    bullet.width = 10;
    bullet.height = 20;
    int bposx = posx + 20;
    int bposy = width;
    int prices[3] = { 10, 10, 10 };
    Texture2D alient_bullet = LoadTexture("textures/alient_ammo.png");
    alient_bullet.width = 10;
    alient_bullet.height = 20;
    int AlientBulletX[30];
    int AlientBulletY[30];
    
    Texture2D avrak = LoadTexture("textures/avrak.png");
    avrak.width = 60;
    avrak.height = 60;
    int AvrakHealth[30];
    
    Texture2D alient_purple = LoadTexture("textures/alient_small_purple.png");
    alient_purple.width = 35;
    alient_purple.height = 35;
    
    while(!WindowShouldClose())
    {
        switch(lscreen)
        {
            case 0:
            {
                if (CheckCollisionPointRec(GetMousePosition(), abb) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) { lscreen = 1; }
                if (CheckCollisionPointRec(GetMousePosition(), stButt) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) { lscreen = 2; }
            } break;
            case 1:
            {
                if (CheckCollisionPointRec(GetMousePosition(), aob) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) { lscreen = 0; }
            } break;
            case 2:
            {
                if (IsKeyPressed(KEY_SPACE) || IsKeyPressed(KEY_ENTER)) {
                    mbsg++;
                }
            } break;
            case 3:
            {
                if (IsKeyDown(KEY_LEFT)) { posx -= speed; }
                if (IsKeyDown(KEY_RIGHT)) { posx += speed; }
                if (Ialient_red[0] == 1) {
                    RedAlientY[0] = 20;
                    RedAlientX[0]++;
                    if (RedAlientX[0] > width) { RedAlientX[0] = -1; }
                    
                    if (AlientBulletY[0] < 70) { AlientBulletX[0] = RedAlientX[0] + alient_red.width/2; }
                    if (AlientBulletY[0] > height) { AlientBulletY[0] = 20; }
                    AlientBulletY[0] += abspeed;
                } else {
                    RedAlientY[0] = -60;
                    AlientBulletY[0] = -60;
                }
                if (Ialient_yellow[0] == 1) {
                    YellowAlientY[0] = 60;
                    YellowAlientX[0]--;
                    if (YellowAlientX[0] < 0) { YellowAlientX[0] = width; }
                    
                    if (AlientBulletY[1] < 100) { AlientBulletX[1] = YellowAlientX[0] + alient_yellow.width/2; }
                    if (AlientBulletY[1] > height) { AlientBulletY[1] = 60; }
                    AlientBulletY[1] += abspeed;
                } else {
                    YellowAlientY[0] = -60;
                    AlientBulletY[1] = -60;
                }
                
                bposy -= bspeed;
                if (bposy <= 0) {
                    bposy = width;
                }
                if (bposy > width - 100) { bposx = posx + rocket.width/2 - 5 ; }
                if (bposx >= RedAlientX[0] && bposx <= RedAlientX[0] + alient_red.width && bposy == RedAlientY[0]) {
                    printf("HIT RED!\n");
                    score += 5;
                    Ialient_red[0] = 0;
                }
                if (bposx >= YellowAlientX[0] && bposx <= YellowAlientX[0] + alient_yellow.width && bposy == YellowAlientY[0]) {
                    printf("HIT YELLOW!\n");
                    score += 5;
                    Ialient_yellow[0] = 0;
                }
                if (AlientBulletX[0] >= posx && AlientBulletX[0] <= posx + rocket.width && AlientBulletY[0] == posy) {
                    printf("CREEP HIT YOU!\n");
                    health -= 1;
                }
                if (AlientBulletX[1] >= posx && AlientBulletX[1] <= posx + rocket.width && AlientBulletY[1] == posy) {
                    printf("CREEP HIT YOU!\n");
                    health -= 1;
                }
                if (Ialient_red[0] == 0 && Ialient_yellow[0] == 0) {
                    mbsg = 0;
                    lscreen = 4;
                }
                if (health <= 0) { lscreen = 999; }
            } break;
            case 4:
            {
                if (IsKeyPressed(KEY_SPACE) || IsKeyPressed(KEY_ENTER)) { mbsg++; }
            } break;
            case 5:
            {
                if (IsKeyPressed(KEY_DOWN) && selIt < 2) { selIt++; }
                if (IsKeyPressed(KEY_UP) && selIt > 0) {selIt--; }
                if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_SPACE)) {
                    if (score >= prices[selIt]) {
                        score -= prices[selIt];
                        if (selIt == 0) { bspeed += 2; }
                        if (selIt == 1) { speed += 2; }
                        if (selIt == 2) { health++; }
                        prices[selIt] += 10;
                    }
                }
                if (IsKeyPressed(KEY_Q)) {
                    mbsg = 0;
                    for (int i = 0; i < 10; i++) {
                        Ialient_red[i] = 1;
                        RedAlientX[i] = width/2 + i;
                        int ralienty = do_random(20, 200);
                        RedAlientY[i] = ralienty;
                    }
                    lscreen = 6;
                }
            } break;
            case 6:
            {
                if (IsKeyPressed(KEY_SPACE) || IsKeyPressed (KEY_ENTER)) { mbsg++; }
            } break;
            case 7:
            {
                if (IsKeyDown(KEY_LEFT)) { posx -= speed; }
                if (IsKeyDown(KEY_RIGHT)) { posx += speed; }

                bposy -= bspeed;
                if (bposy <= 0) {
                    bposy = width;
                }
                if (bposy > width - 100) { bposx = posx + rocket.width/2 - 5; }
                
                for (int i = 0; i < 5; i++) {
                    int alspeed = do_random(1, 10);
                    RedAlientX[i] += alspeed;
                    if (RedAlientX[i] > width) {
                        RedAlientY[i] = do_random(20, 120);
                        RedAlientX[i] = 0;
                    }
                    if (bposx >= RedAlientX[i] && bposx <= RedAlientX[i] + alient_red.width && bposy <= 119)
                    {
                        printf("HIT RED!\n");
                        Ialient_red[i] = 0;
                        RedAlientY[i] = -60;
                        AlientBulletY[i] = -60;
                        score += 5;
                    }
                    if (Ialient_red[i] == 1) {
                        if (AlientBulletY[i] < 70) { AlientBulletX[i] = RedAlientX[i] + alient_red.width/2; }
                        if (AlientBulletY[i] > height) { AlientBulletY[i] = 20; }
                        AlientBulletY[i] += abspeed;
                    }
                    if (AlientBulletX[i] >= posx && AlientBulletX[i] <= posx + rocket.width && AlientBulletY[i] == posy) {
                        printf("CREEP HIT YOU!\n");
                        health -= 1;
                    }
                }
                if (Ialient_red[0] == 0 && Ialient_red[1] == 0 && Ialient_red[2] == 0 && Ialient_red[3] == 0 && Ialient_red[4] == 0)
                {
                    mbsg = 0;
                    lscreen = 8;
                }

                if (health <= 0) { lscreen = 999; }
            } break;
            case 8:
            {
                if (IsKeyPressed(KEY_SPACE) || IsKeyPressed (KEY_ENTER)) { mbsg++; }
            } break;
            case 9:
            {
                if (IsKeyDown(KEY_LEFT)) { posx -= speed; }
                if (IsKeyDown(KEY_RIGHT)) { posx += speed; }
                bposy -= bspeed;
                if (bposy <= 0) {
                    bposy = width;
                }
                if (bposy > width - 100) { bposx = posx + rocket.width/2 - 5; }
            } break;
        }
        BeginDrawing();
        ClearBackground(BLUE);
        switch(lscreen)
        {
            case 0:
            {
                DrawText("RADIANT CLONE", width/2 - 200, 10, 50, WHITE);
                DrawText("START", width/2 - 100, 120, 35, WHITE);
                // DrawText("ADVANCEMENTS", width/2 - 200, 155, 35, WHITE);
                DrawText("ABOUT", width/2 - 100, 290, 35, WHITE);
            } break;
            case 1:
            {
                DrawText("RADIANT CLONE v0.1", width/2 - 250, 10, 50, WHITE);
                DrawText("Made by glowiak in C/Raylib", width/2 - 300, 80, 35, WHITE);
                DrawText("OK", 10, height - 20, 20, WHITE);
            } break;
            case 2:
            {
                DrawTexture(bg, 0, 0, BLUE); // the background
                DrawTexture(rocket, posx, posy, WHITE); // the player
                
                if (mbsg == 0) {
                    DrawRectangle(100,100,700,400, BLUE);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("COMMANDER NORTON", 103, 103, 20, WHITE);
                    DrawText("THIS IS COMMANDER NORTON", 120, 120, 15, WHITE);
                    DrawText("TO FIRST SPACE SERGEANT MAX BLASTER", 120, 140, 15, WHITE);
                    DrawText("HOW ARE YOU DOING OUT THERE, SON?", 120, 160, 15, WHITE);
                }
                if (mbsg == 1) {
                    DrawRectangle(100,100,700,400, GREEN);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("MAX BLASTER", 103, 103, 20, WHITE);
                    DrawText("FLYING STRAIGHT", 120, 120, 15, WHITE);
                    DrawText("ABOVE THE STEADY", 120, 140, 15, WHITE);
                    DrawText("STREAM OF STARS, SIR.", 120, 160, 15, WHITE);
                }
                if (mbsg == 2) {
                    DrawRectangle(100,100,700,400, BLUE);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("COMMANDER NORTON", 103, 103, 20, WHITE);
                    DrawText("MAY I CHEER YOU, MAX?", 120, 120, 15, WHITE);
                }
                if (mbsg == 3) {
                    DrawRectangle(100,100,700,400, GREEN);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("MAX BLASTER", 103, 103, 20, WHITE);
                    DrawText("YOU CAN ALWAYS TRY,", 120, 120, 15, WHITE);
                    DrawText("SIR.", 120, 140, 15, WHITE);
                }
                if (mbsg == 4) {
                    DrawRectangle(100,100,700,400, BLUE);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("COMMANDER NORTON", 103, 103, 20, WHITE);
                    DrawText("ALRIGHT, THEN TRY TO GUESS WHO I'M MIMING.", 120, 120, 15, WHITE);
                    DrawText("*HUMANS I COME TO KILL YOU ALL*", 120, 140, 15, WHITE);
                    DrawText("WHO WAS THAT, MAX?", 120, 160, 15, WHITE);
                }
                if (mbsg == 5) {
                    DrawRectangle(100,100,700,400, GREEN);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("MAX BLASTER", 103, 103, 20, WHITE);
                    DrawText("SOMETHING TELLS ME", 120, 120, 15, WHITE);
                    DrawText("THAT YOU'RE MIMING THIS CREEP", 120, 140, 15, WHITE);
                    DrawText("THAT INVADED US IN '80s, SIR.", 120, 160, 15, WHITE);
                }
                if (mbsg == 6) { // supercreep joins the game
                    DrawRectangle(100,100,700,400, RED);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("SUPERCREEP", 103, 103, 20, WHITE);
                    DrawText("*HUMANS I COME TO KILL YOU ALL*", 120, 120, 15, WHITE);
                }
                if (mbsg == 7) {
                    DrawRectangle(100,100,700,400, GREEN);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("MAX BLASTER", 103, 103, 20, WHITE);
                    DrawText("SECOND TIME YOU WERE MORE REALISTIC", 120, 120, 15, WHITE);
                    DrawText("LIKE IT WAS NOT YOU, SIR.", 120, 140, 15, WHITE);
                }
                if (mbsg == 8) {
                    DrawRectangle(100,100,700,400, BLUE);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("COMMANDER NORTON", 103, 103, 20, WHITE);
                    DrawText("MAX! THIS WASN'T ME!", 120, 120, 15, WHITE);
                    DrawText("ALIENTS ARE EVERYWHERE!", 120, 140, 15, WHITE);
                    DrawText("YOU ARE OUR ONLY HOPE.", 120, 160, 15, WHITE);
                }
                if (mbsg == 9) {
                    DrawRectangle(100,100,700,400, RED);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("SUPERCREEP", 103, 103, 20, WHITE);
                    DrawText("YOUR CHANGES ARE ZERO.", 120, 120, 15, WHITE);
                }
                if (mbsg > 9) {
                    Ialient_red[0] = 1;
                    Ialient_yellow[0] = 1;
                    lscreen = 3;
                }
            } break;
            case 3:
            {
                DrawTexture(bg, 0, 0, BLUE); // the background
                if (health >= 1) {
                    DrawTexture(heart, width - 50, 10, WHITE);
                }
                if (health >= 2) {
                    DrawTexture(heart, width - 80, 10, WHITE);
                }
                if (health >= 3) {
                    DrawTexture(heart, width - 110, 10, WHITE);
                }
                if (health >= 4) {
                    DrawTexture(heart, width - 140, 10, WHITE);
                }
                if (health >= 5) {
                    DrawTexture(heart, width - 170, 10, WHITE);
                }
                if (health >= 6) { // allow 5 hearts at maximum
                    health = 5;
                }
                DrawTexture(bullet, bposx, bposy, ORANGE); // the bullet
                DrawTexture(alient_red, RedAlientX[0], RedAlientY[0], RED); // red alient
                DrawTexture(alient_yellow, YellowAlientX[0], YellowAlientY[0], YELLOW); // yellow alient
                DrawTexture(rocket, posx, posy, WHITE); // the player
                DrawText(TextFormat("Score: %d", score), 1, 1, 20, WHITE);
                DrawTexture(alient_bullet, AlientBulletX[0], AlientBulletY[0], RED); // the red alient's bullet
                DrawTexture(alient_bullet, AlientBulletX[1], AlientBulletY[1], YELLOW); // the yellow's one
            } break;
            case 4:
            {
                DrawTexture(bg, 0, 0, BLUE); // the background
                DrawTexture(rocket, posx, posy, WHITE); // the player
                if (mbsg == 0) {
                    DrawRectangle(100,100,700,400, BLUE);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("COMMANDER NORTON", 103, 103, 20, WHITE);
                    DrawText("HOLD ON MAX, YOU'RE GETTING CLOSER TO", 120, 120, 15, WHITE);
                    DrawText("A PRIVATE GUNSHOP. ALL OUR MILITARY BASES", 120, 140, 15, WHITE);
                    DrawText("ARE BEING DESTROYED BY ALIENTS.", 120, 160, 15, WHITE);
                    DrawText("THIS MAY BE THE ONLY WAY FOR YOU TO UPGRADE AMMO.", 120, 180, 15, WHITE);
                    DrawText("BUT YOU WILL NEED TO PAY FROM YOUR MONEY.", 120, 200, 15, WHITE);
                }
                if (mbsg == 1) {
                    DrawRectangle(100,100,700,400, GREEN);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("MAX BLASTER", 103, 103, 20, WHITE);
                    DrawText("NO PROBLEM, SIR.", 120, 120, 15, WHITE);
                    DrawText("I WILL PAY IN ALIENT SCUM.", 120, 140, 15, WHITE);
                }
                if (mbsg > 1) {
                    lscreen = 5;
                }
            } break;
            case 5:
            {
                DrawRectangle(0, 0, width, height, BLACK);
                DrawRectangle(0, 0, width, 50, PURPLE);
                DrawText("SHOP", 10, 10, 35, WHITE);
                DrawText(TextFormat("CASH: %d", score), width - 150, 10, 20, WHITE);
                
                if (selIt == 0)
                { DrawText(TextFormat("GUN (%d points)", prices[0]), 10, 60, 20, WHITE); }
                else { DrawText(TextFormat("GUN (%d points)", prices[0]), 10, 60, 20, GRAY); }
                if (selIt == 1)
                { DrawText(TextFormat("ENGINE (%d points)", prices[1]), 10, 80, 20, WHITE); }
                else { DrawText(TextFormat("ENGINE (%d points)", prices[1]), 10, 80, 20, GRAY); }
                if (selIt == 2)
                { DrawText(TextFormat("EXTRA LIVE (%d points)", prices[2]), 10, 100, 20, WHITE); }
                else { DrawText(TextFormat("EXTRA LIVE (%d points)", prices[2]), 10, 100, 20, GRAY); }
                
                DrawText("PRESS Q TO EXIT", 10, height - 20, 20, GRAY);
            } break;
            case 6:
            {
                DrawTexture(bg, 0, 0, BLUE); // the background
                DrawTexture(rocket, posx, posy, WHITE); // the player
                if (mbsg == 0) {
                    DrawRectangle(100,100,700,400, BLUE);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("COMMANDER NORTON", 103, 103, 20, WHITE);
                    DrawText("ALRIGHT MAX,", 120, 120, 15, WHITE);
                    DrawText("MAKE THIS CREEP CRY.", 120, 140, 15, WHITE);
                }
                if (mbsg == 1) {
                    DrawRectangle(100,100,700,400, GREEN);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("MAX BLASTER", 103, 103, 20, WHITE);
                    DrawText("AY AY, SIR.", 120, 120, 15, WHITE);
                }
                if (mbsg > 1) { lscreen = 7; }
            } break;
            case 7:
            {
                DrawTexture(bg, 0, 0, BLUE);
                if (health >= 1) {
                    DrawTexture(heart, width - 50, 10, WHITE);
                }
                if (health >= 2) {
                    DrawTexture(heart, width - 80, 10, WHITE);
                }
                if (health >= 3) {
                    DrawTexture(heart, width - 110, 10, WHITE);
                }
                if (health >= 4) {
                    DrawTexture(heart, width - 140, 10, WHITE);
                }
                if (health >= 5) {
                    DrawTexture(heart, width - 170, 10, WHITE);
                }
                if (health >= 6) { // allow 5 hearts at maximum
                    health = 5;
                }
                DrawTexture(bullet, bposx, bposy, ORANGE); // the bullet
                DrawTexture(rocket, posx, posy, WHITE); // the player
                DrawText(TextFormat("Score: %d", score), 1, 1, 20, WHITE);
                for (int i = 0; i < 5; i++) {
                    if (Ialient_red[i] == 1) { DrawTexture(alient_red, RedAlientX[i], RedAlientY[i], RED); }
                    if (Ialient_red[i] == 1) { DrawTexture(alient_bullet, AlientBulletX[i], AlientBulletY[i], RED); }
                }
            } break;
            case 8:
            {
                DrawTexture(bg, 0, 0, BLUE); // the background
                DrawTexture(rocket, posx, posy, WHITE); // the player
                if (mbsg == 0) {
                    DrawRectangle(100,100,700,400, BLUE);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("COMMANDER NORTON", 103, 103, 20, WHITE);
                    DrawText("GREAT JOB, MAX.", 120, 120, 15, WHITE);
                    DrawText("I KNOW THAT YOU HAVE MUCH ON YOUR HEAD, BUT OUR", 120, 140, 15, WHITE);
                    DrawText("INTELLIGENCE LEAKED THAT THEY SENT TWO HEAVY BOMBARDIERS AGAINST YOU.", 120, 160, 15, WHITE);
                }
                if (mbsg == 1) {
                    DrawRectangle(100,100,700,400, GREEN);
                    DrawRectangle(120,120,660,360, BLACK);
                    DrawText("MAX BLASTER", 103, 103, 20, WHITE);
                    DrawText("THE MORE THE MERRIER.", 120, 120, 15, WHITE);
                }
                if (mbsg > 1) {
                    AvrakHealth[0] = 5;
                    AvrakHealth[1] = 5;
                    lscreen = 9;
                }
            } break;
            case 9:
            {
                DrawTexture(bg, 0, 0, BLUE); // the background
                DrawTexture(rocket, posx, posy, WHITE); // the player
                DrawTexture(bullet, bposx, bposy, ORANGE);
            } break;
            case 999:
            {
                DrawRectangle(0, 0, width, height, BLACK);
                DrawTexture(avrak, width/2 - 60, 10, RED);
                DrawText("THE SUPERCREEP HAS KILLED YOU.", width/2 - 250, 100, 25, RED);
            } break;
        }
        
        EndDrawing();
    }
    
    CloseWindow();
    return 0;
}
int do_random(int l, int u)
{
    return (rand() % (u - l - 1)) + l;
}
