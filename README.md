# Radiant Clone

A clone of HEXAGE's game Radiant.

Made in The C Programming Language (because it's so superior, only assembly and directly writing machine code are better) with use of the Raylib graphics library.

### Compiling

Ah, this is the plus of Java - I am not able to compile this program for every platform available.

So you have to compile it yourself:

	make

Or wait, it's only one file, so:

	gcc -lraylib -O3 src/main.c -o radiant-clone

does the same task. A note that this won't work with clang.

You can also install it (though it's not recommended) by typing:

	make PREFIX=/usr install

You also need to have Raylib installed for this to work.

### Textures

These textures are not the textures from the Radiant game and are not property of anyone. I simply redrawed it pixel by pixel (with sometimes bad result) in GIMP.

### License

This clone has no license. You can do whatever you want with this code and textures, but at your own risk.

## Frequently Asked Questions

### Why C?

	but they all use shortcuts like 3d engines or languages with classes and garbage collection and namespaces and things you don't really need
	 - jdh, https://www.youtube.com/watch?v=4O0_-1NaWnY&t=32s

This is the explaination.

### But raylib is bloated.

shut up
